package misMetodos;

import misApis.ColaPrioridadTDA;
import misApis.ConjuntoTDA;
import misImplementaciones.dinamicas.ColaPrioridad;
import misImplementaciones.dinamicas.Conjunto;


public class MetodosConjunto {

	/**  @Tarea Ordenar un conjunto
     *  @Par�metros ConjuntoTDA c1
     *  @Devuelve ColaPrioridadTDA
     *  @Precondici�n El conjunto c1 debe estar inicializado.
     *  @Postcondici�n El conjunto c1 queda ordenado en una cola con prioridad
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/
	public static ColaPrioridadTDA ordenarConjunto(ConjuntoTDA c1) {
		ColaPrioridadTDA conjOrdenado = new ColaPrioridad();
		conjOrdenado.inicializar();
		int elemento;
		while(!c1.estaVacio()) {
			 elemento = c1.elegir();
			 conjOrdenado.acolarPrioridad(1, elemento);
			 c1.sacar(elemento);
		}
		return conjOrdenado;
	}
	
	/**  @Tarea Imprimir un conjunto previamente ordenado en una cola con prioridad
     *  @Par�metros ColaPrioridadTDA
     *  @Devuelve void
     *  @Precondici�n El conjunto c debe estar inicializado.
     *  @Postcondici�n Se imprime el conjunto c
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/
	public static void imprimirConjuntoOrdenado(ColaPrioridadTDA c) {
		while(!c.estaVacia()) {
			System.out.println(c.prioridad());
			c.desacolar();
		}
		
		
	}
	
    /**  @Tarea Copiar los elementos de un conjunto en otro.
     *  @Par�metros ConjuntoTDA c1, ConjuntoTDA c2.
     *  @Devuelve Void.
     *  @Precondici�n El conjunto c1 y el conjunto c2 deben estar inicializado.
     *  @Postcondici�n El conjunto c2, contiene ahora todos los elementos de c1.
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/
    public static void copiarConjunto(ConjuntoTDA c1, ConjuntoTDA c2){
        ConjuntoTDA aux = new Conjunto();
        aux.inicializar();
        int valor;
        while(!c1.estaVacio()){
            valor = c1.elegir();
            aux.agregar(valor);
            c1.sacar(valor);
        }
        while(!aux.estaVacio()){
            valor = aux.elegir();
            c1.agregar(valor);
            c2.agregar(valor);
            aux.sacar(valor);
        }
    }

    
    /**  @Tarea Imprimir los elementos de un conjunto.
     *  @Par�metros ConjuntoTDA c.
     *  @Devuelve Void.
     *  @Precondici�n El conjunto c debe estar inicializado.
     *  @Postcondici�n Se muestran los elementos del conjunto.
     *  @Costo Est� dado por la cantidad de elementos de c, su costo es O(n)
     *  **/
    public static void imprimirConjunto(ConjuntoTDA c) {
        int valor;
        ConjuntoTDA aux = new Conjunto();
        aux.inicializar();
        copiarConjunto(c,aux);
        while (!aux.estaVacio()) {
            valor = aux.elegir();
            System.out.println("    " + valor);
            aux.sacar(valor);
        }
    }

    /**  @Tarea Calcular la cantidad de elementos de un conjunto.
     *  @Par�metros ConjuntoTDA c.
     *  @Devuelve Int.
     *  @Precondici�n El conjunto c debe estar inicializado.
     *  @Postcondici�n Se devuelve el tama�o del conjunto.
     *  @Costo Est� dado por la cantidad de elementos de c, su costo es O(n)
     *  **/
    public static int tamanio(ConjuntoTDA c) {
        int valor, cantidadElem = 0;
        ConjuntoTDA aux = new Conjunto();
        aux.inicializar();
        copiarConjunto(c,aux);
        while (!aux.estaVacio()) {
            valor = aux.elegir();
            aux.sacar(valor);
            cantidadElem++;
        }
        return cantidadElem;
    }

    /**  @Tarea Verificar si los elementos de un conjunto est�n contenidos en otro conjunto.
     *  @Par�metros ConjuntoTDA c.
     *  @Devuelve Boolean.
     *  @Precondici�n El conjunto c1 y c2 deben estar inicializados y no vac�os.
     *  @Postcondici�n Se devuelve true o false, seg�n si c1 efectivamente est� contenido en c2.
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/
    public static boolean contiene(ConjuntoTDA c1, ConjuntoTDA c2){
        int elegido;
        ConjuntoTDA aux_c1 = new Conjunto();
        aux_c1.inicializar();
        copiarConjunto(c1,aux_c1);
        while (!aux_c1.estaVacio()){
            elegido = aux_c1.elegir();
            if(!c2.pertenece(elegido))
                return false;
            aux_c1.sacar(elegido);
        }
        return true;
    }

    /**  @Tarea Devolver elementos contenidos en ambos conjuntos.
     *  @Par�metros ConjuntoTDA c1, ConjuntoTDA c2.
     *  @Devuelve ConjuntoTDA
     *  @Precondici�n El conjunto c1 y c2 deben estar inicializados y no vac�os.
     *  @Postcondici�n Se devuelve un conjunto que contiene la intersecci�n entre los conjuntos recibidos por par�metro
     *  @Costo Est� dado por la cantidad de elementos de c1, su costo es O(n)
     *  **/

    public static ConjuntoTDA interseccion(ConjuntoTDA c1, ConjuntoTDA c2) {
        int elemento_c1;
        ConjuntoTDA c1Aux = new Conjunto();
        ConjuntoTDA interseccion = new Conjunto();
        c1Aux.inicializar();
        interseccion.inicializar();
        MetodosConjunto.copiarConjunto(c1,c1Aux);

        while (!c1Aux.estaVacio()){
            elemento_c1 = c1Aux.elegir();
            if(c2.pertenece(elemento_c1))
                interseccion.agregar(elemento_c1);
            c1Aux.sacar(elemento_c1);
        }
        return interseccion;
    }

    
    /**  @Tarea Dado un conjunto c1 y c2 recibidos por par�metro, devuelve la uni�n de ambos.
     *  @Par�metros ConjuntoTDA, ConjuntoTDA.
     *  @Devuelve ConjuntoTDA.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve un conjunto con la union de otros 2.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibidos, su costo es O(n)
     *  **/
    public static ConjuntoTDA union(ConjuntoTDA c1, ConjuntoTDA c2) {
        int elemento_c1;
        ConjuntoTDA c1Aux = new Conjunto();
        ConjuntoTDA union = new Conjunto();
        c1Aux.inicializar();
        union.inicializar();
        MetodosConjunto.copiarConjunto(c1,c1Aux);
        MetodosConjunto.copiarConjunto(c2,union);

        while (!c1Aux.estaVacio()){
            elemento_c1 = c1Aux.elegir();
            if(!union.pertenece(elemento_c1))
                union.agregar(elemento_c1);
            c1Aux.sacar(elemento_c1);
        }
        return union;
    }

    
    /**  @Tarea Dado un conjunto c1 y c2 recibidos por par�metro, devuelve un conjunto con las materias que son propias de c1 pero que no est�n en c2 (o sea la diferencia).
     *  @Par�metros ConjuntoTDA, ConjuntoTDA.
     *  @Devuelve ConjuntoTDA.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve el tama�o del conjunto.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibidos, su costo es O(n)
     *  **/
    public static ConjuntoTDA diferencia(ConjuntoTDA c1, ConjuntoTDA c2) {
        int elemento_c1;
        ConjuntoTDA c1Aux = new Conjunto();
        ConjuntoTDA diferencia = new Conjunto();
        c1Aux.inicializar();
        MetodosConjunto.copiarConjunto(c1,c1Aux);
        MetodosConjunto.copiarConjunto(c1,diferencia);

        while (!c1Aux.estaVacio()){
            elemento_c1 = c1Aux.elegir();
            if(c2.pertenece(elemento_c1))
                diferencia.sacar(elemento_c1);
            c1Aux.sacar(elemento_c1);
        }
        return diferencia;
    }
    
    

    /**  @Tarea Dado un conjunto c1 y c2 recibidos por par�metro, devuelve un conjunto con las materias que son propias de c1 y tambi�n c2 pero sin su intersecci�n
     *  @Par�metros ConjuntoTDA, ConjuntoTDA.
     *  @Devuelve ConjuntoTDA.
     *  @Precondici�n Los conjuntos deben estar inicializados
     *  @Postcondici�n Se devuelve el conjunto que contiene la diferencia sim�trica entre ambos conjuntos.
     *  @Costo  Esta dado por la cantidad de materias de los conjuntos recibidos, su costo es O(n)
     *  **/
    public static ConjuntoTDA diferenciaSimetrica(ConjuntoTDA c1, ConjuntoTDA c2) {
        ConjuntoTDA diferenciaSimetrica;
        ConjuntoTDA union;
        ConjuntoTDA interseccion;

        union = union(c1,c2);
        interseccion = interseccion(c1,c2);
        diferenciaSimetrica = diferencia(union,interseccion);

        return diferenciaSimetrica;
    }
}

